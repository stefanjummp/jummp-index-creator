<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Create Index</title>
        <meta name="layout" content="main" />
    </head>
    <body>
        <h1>Create Index</h1>
        <div id="index">
            <g:form method="POST" action="createIndex">
                <table>
                    <tr>
                        <th><label for="username">Username</label></th>
                        <td><input type="text" id="username" name="username"/></td>
                    </tr>
                    <tr>
                        <th><label for="password">Password</label></th>
                        <td><input type="password" id="password" name="password"/></td>
                    </tr>
                    <tr>
                        <th><label for="index-path">Local Path to Index</label></th>
                        <td><input type="text" id="index-path" name="path" value="/path/to/folder"/></td>
                    </tr>
                </table>
                <div class="buttons">
                    <input type="reset" value="Cancel"/>
                    <input type="submit" value="Start"/>
                </div>
            </g:form>
        </div>
    </body>
</html>
