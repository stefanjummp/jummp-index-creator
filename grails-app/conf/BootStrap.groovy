class BootStrap {
    /**
     * Dependency injection of remoteJummpApplicationAdapter
     */
    def remoteJummpApplicationAdapter
    def grailsApplication

    def init = { servletContext ->
        grailsApplication.config.jummpCore = remoteJummpApplicationAdapter.getJummpConfig("index creator")
    }
    def destroy = {
    }
}
